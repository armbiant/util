#include "file.h"

#include <iostream>
#include <algorithm>
#include <filesystem>
#include <fstream>
#include <sys/stat.h>
#include <pwd.h>
#include <unistd.h>
#include <spdlog/spdlog.h>

using namespace Halo;

File::File()
{}

File::~File()
= default;

const bool File::path_exists(const std::string& path)
{
    struct stat buffer{};
    return (stat(path.c_str(), &buffer) == 0);
}

void File::path_exists_with_error(const std::string& path)
{
    if (!path_exists(path)) // Verify that file exists
    {
        spdlog::error("{} - Path does not exist", path);
        exit(0);
    }
}

const std::string File::home_dir()
{
    const char *homedir;

    if ((homedir = getenv("HOME")) == nullptr) {
        homedir = getpwuid(getuid())->pw_dir;
    }

    return (std::string)homedir;
}

const std::pair<std::string, std::string> File::separate_extension(const std::string& input_path, const bool& absolute_path)
{
    const std::string extension = get_extension(input_path);

    std::filesystem::path modified_path(input_path);
    if (absolute_path)
    {
        modified_path = std::filesystem::canonical(modified_path);
    }
    
    modified_path.replace_extension("");

    return std::make_pair(modified_path, extension);
}

const std::string File::append_to_filename(const std::string& input, const std::string& suffix)
{
    const std::pair<std::string, std::string> separated_path = separate_extension(input, false);
    return separated_path.first + suffix + separated_path.second;
}

void File::create_dir(const std::string& path)
{
    if (!path_exists(path))
    {
        std::filesystem::create_directories(path);
    }
}

void File::create_file(const std::string& path)
{
    std::filesystem::path fs_path{path};
    std::filesystem::create_directories(fs_path.parent_path());
    std::ofstream ofs(fs_path);
    ofs.close();
}

const std::string File::get_extension(const std::string& path)
{
    return std::filesystem::path(path).extension();
}

const bool File::check_extension(const std::string& path, const std::string& desired_extension)
{
    const std::string path_ext = get_extension(path);
    const bool extensions_match = (path_ext == desired_extension);
    if (!extensions_match)
    {
        spdlog::debug("[{}] Detected extension: {}", __FILE__, path_ext);
    }
    
    return extensions_match;
}

const std::string File::make_output_filename(const std::string& input, const std::string& output, const bool& same_extension)
{
    std::string output_filename = input;
    const std::pair<std::string, std::string> separated_input_path = separate_extension(input, true);

    if (output == "")
    {
        output_filename = separated_input_path.first + "_output" + separated_input_path.second;
    }
    else
    {
        if (same_extension)
        {
            const std::pair<std::string, std::string> separated_output_path = separate_extension(output, false);
            output_filename = separated_output_path.first + separated_input_path.second;
        }
        else
        {
            output_filename = output;
        }
    }

    return output_filename;
}

const int File::count_files(const std::string& dir_path)
{
    // Check that directory exists
    path_exists_with_error(dir_path);

    // Count files
    auto dir_iter = std::filesystem::directory_iterator(dir_path);

    int file_count = std::count_if(
        begin(dir_iter),
        end(dir_iter),
        [](auto& entry) { return entry.is_regular_file(); }
    );

    return file_count;
}

const bool File::remove(const std::string& path)
{
    const bool removed = std::filesystem::remove(path);
    return removed;
}
