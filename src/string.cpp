#include "string.h"

#include <algorithm>
#include <cctype>
#include <ctime>

using namespace Halo;

String::String()
{}

String::~String()
= default;

const std::string String::toLower(const std::string& input)
{
    std::string lower = input;
    std::transform(lower.begin(), lower.end(), lower.begin(), [](unsigned char c){ return std::tolower(c); });
    return lower;
}

const std::string String::toUpper(const std::string& input)
{
    std::string upper = input;
    std::transform(upper.begin(), upper.end(), upper.begin(), [](unsigned char c){ return std::toupper(c); });
    return upper;
}

const std::string String::pad_index(const int& index, const int& padding)
{
    const std::string padding_string = "%0" + std::to_string(padding) + "d";
    char index_str[12];
    sprintf(index_str, padding_string.c_str(), index);
    std::string index_string(index_str);
    return index_string;
}

const std::string String::date_time_string()
{
    time_t now = time(0);
    tm *ltm = localtime(&now);

    const std::string year_string = pad_index(ltm->tm_year-100, 2);
    const std::string month_string = pad_index(ltm->tm_mon+1, 2);
    const std::string date_string = pad_index(ltm->tm_mday, 2);

    const std::string hour_string = pad_index(ltm->tm_hour, 2);
    const std::string minute_string = pad_index(ltm->tm_min, 2);
    const std::string second_string = pad_index(ltm->tm_sec, 2);

    const std::string date_time_string = year_string + month_string + date_string + "_" + hour_string + minute_string + second_string;
    return date_time_string;
}
