#ifndef HALO_STRING_H
#define HALO_STRING_H

#include <string>

namespace Halo
{

class String
{
public:
    String();
    ~String();

    static const std::string toLower(const std::string& input);
    static const std::string toUpper(const std::string& input);

    static const std::string pad_index(const int& index, const int& padding);
    static const std::string date_time_string();
};

}

#endif
