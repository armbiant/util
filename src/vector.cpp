#include "vector.h"

#include <cmath>

using namespace Halo;

template <typename input_type>
const VectorContains Vector::contains(const std::vector<input_type>& vector, const input_type &element)
{
    VectorContains result{false, -1};

    for (int i = 0; i < vector.size(); i++)
    {
        if (vector.at(i) == element)
        {
            result.contains = true;
            result.index = i;
            break;
        }
    }
    return result;
}
