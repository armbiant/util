#ifndef HALO_EIGEN_H
#define HALO_EIGEN_H

// Make intellisense work with Eigen on Arm
#if __INTELLISENSE__
#undef __ARM_NEON
#undef __ARM_NEON__
#endif

#include <cmath>
#include <Eigen/Dense>

#endif