#ifndef HALO_FILE_H
#define HALO_FILE_H

#include <string>
#include <utility>

namespace Halo
{

class File
{
public:
    File();
    ~File();

    static const bool path_exists(const std::string& path);         // Check if file of directory exists or not
    static void path_exists_with_error(const std::string& path);
    static const std::string home_dir();                            // Return home directory path
    static const std::pair<std::string, std::string> separate_extension(const std::string& input_path, const bool& absolute_path = true);
    static const std::string append_to_filename(const std::string& input, const std::string& suffix);
    
    static void create_dir(const std::string& path);
    static void create_file(const std::string& path);

    static const std::string get_extension(const std::string& path);
    static const bool check_extension(const std::string& path, const std::string& desired_extension);
    static const std::string make_output_filename(const std::string& input, const std::string& output = "", const bool& same_extension = true);
    
    static const int count_files(const std::string& path);
    static const bool remove(const std::string& path);
};

}

#endif
