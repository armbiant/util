#ifndef HALO_VECTOR_H
#define HALO_VECTOR_H

#include <iostream>
#include <vector>
#include <numeric>

template <typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& v)
{
    os << "[";
    for (int i = 0; i < v.size(); ++i)
    {
        os << v.at(i);
        if (i != v.size() - 1)
        {
            os << ", ";
        }
    }
    os << "]";
    return os;
}

namespace Halo
{

struct VectorContains
{
    bool contains;
    int index;
};

class Vector
{
public:
    // Check if element is inside vector or not, return index of element as well
    template <typename input_type>
    static const VectorContains contains(const std::vector<input_type>& vector, const input_type& element);

    // Compute average of values in vector
    template <typename vector_type>
    static inline const vector_type average(const std::vector<vector_type>& vector)
    {
        if(vector.empty())
        {
            return 0;
        }

        auto const count = static_cast<vector_type>(vector.size());
        return std::reduce(vector.begin(), vector.end()) / count;
    }

    // Scale elements in vector to value
    template <typename T1>
    static inline void scale_elements(std::vector<T1>& vector, const T1 scale_factor)
    {
        // Find maximum element in vector
        const T1 max_elem = *max_element(vector.begin(), vector.end());
        const T1 min_elem = *min_element(vector.begin(), vector.end());

        float ratio;
        if (std::abs(min_elem) > std::abs(max_elem))
        {
            ratio = (float)scale_factor/(float)std::abs(min_elem);
        }
        else if (std::abs(min_elem) <= std::abs(max_elem))
        {
            ratio = (float)scale_factor/(float)std::abs(max_elem);
        }
        
        std::transform(vector.begin(), vector.end(), vector.begin(), [ratio](T1& c){ return c*ratio; });
    }
};

}   // namespace Halo

#endif