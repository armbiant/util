#include "src/file.h"

#include <spdlog/spdlog.h>
#include <cxxopts.hpp>

using namespace Halo;

struct Args
{
    std::string input;
};

const Args parse_args(int argc, char* argv[])
{
    cxxopts::Options options(__FILE__);

    options.add_options()
        ("i, input_file", "Input image", cxxopts::value<std::string>())
        ("h,help", "Print usage");

    auto args = options.parse(argc, argv);

    if (argc < 2 || args.count("help"))
    {
        std::cout << options.help() << std::endl;
        exit(0);
    }

    const std::string input_file = args["input_file"].as<std::string>();

    File::path_exists_with_error(input_file);

    const Args arguments{input_file};

    return arguments;
}

int main(int argc, char* argv[])
{
    const Args args = parse_args(argc, argv);

    const std::pair<std::string, std::string> sep_path = File::separate_extension(args.input, false);

    std::cout << "Path: " << sep_path.first << std::endl;
    std::cout << "Ext: " << sep_path.second << std::endl;
    
    return 0;
}